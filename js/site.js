// Firebase config
let firebaseConfig = {
  apiKey: "AIzaSyAqZLd6iHdHgue0ys3S5734s_MakQOrJO4",
  authDomain: "seed-christmas.firebaseapp.com",
  databaseURL: "https://seed-christmas.firebaseio.com",
  projectId: "seed-christmas",
  storageBucket: "seed-christmas.appspot.com",
  messagingSenderId: "432195017504"
};
firebase.initializeApp(firebaseConfig);

// Define variables and functions here
const $goingCards = $('#going');
const $goingCount = $('.item[data-tab="going"] .count');
const $notGoingCards = $('#notGoing');
const $notGoingCount = $('.item[data-tab="not going"] .count');
const $maybeCards = $('#maybe');
const $maybeCount = $('.item[data-tab="maybe"] .count');
const $foodMenuList = $('#foodMenu .ui.list');
const firestore = firebase.firestore();
const participantsRef = firestore.collection('participants');
const foodMenuRef = firestore.collection('foodMenu');
const $scrollTopBtn = $('#scrollTopBtn');
let c1 = c2 = c3 = 0;

function attendingText(x) {
  switch (x) {
    case 1:
      return "Going"
    case 2:
      return "Not Going"
    default:
      return "Maybe"
  }
}

function randomColorHex()
{
  return (Math.random() * 0xFFFFFF << 0).toString(16);
}

function contrastColor(color) 
{
  let r = parseInt('0x' + color.substring(0, 2));
  let g = parseInt('0x' + color.substring(2, 4));
  let b = parseInt('0x' + color.substring(4, 6));
  let d = 0;

  // Counting the perceptive luminance - human eye favors green color... 
  let a = 1 - ( 0.299 * r + 0.587 * g + 0.114 * b)/255;

  if (a < 0.5)
     d = 0; // bright colors - black font
  else
     d = 240; // dark colors - white font

  return d.toString(16) + d.toString(16) + d.toString(16);
}

function getAttendanceRealtimeUpdates() {
  participantsRef.orderBy('name').onSnapshot(function updateHandler(ss) {
    $goingCards.empty();
    $goingCount.empty();
    $notGoingCards.empty();
    $notGoingCount.empty();
    $maybeCards.empty();
    $maybeCount.empty();
    c1 = c2 = c3 = 0;
    
    // loop over each participant
    ss.forEach(function eachDoc(doc) {
      let participant = doc.data();
      
      let imageUrl;
      let telegramUrl;
      if (participant.imageUrl != null && participant.imageUrl !== '') {
        imageUrl = participant.imageUrl;
      } else {
        let bgColor;
        let isDark = false;
        // loop till a dark color comes up
        while (!isDark) {
          bgColor = randomColorHex();
          isDark = contrastColor(bgColor) != '000';
        }
        imageUrl = `https://ui-avatars.com/api/?name=${participant.name}&background=${bgColor}&color=${contrastColor(bgColor)}&size=1024&rounded=false`;
      }
      
      // handle null telegram names
      if (participant.telegramUsername != null && participant.telegramUsername !== '') {
        telegramUrl = `<a href="https://telegram.me/${participant.telegramUsername}">${participant.telegramUsername}</a>`;
      } else {
        telegramUrl = 'N.A.';
      }
      
      let cardHtml = `<div class="card">\
          <div class="image">\
            <img src="${imageUrl}">\
          </div>\
          <div class="content">\
            <p class="header">${participant.name}</p>\
            <div class="meta">\
              <span><i class="telegram icon"></i> ${telegramUrl}</span>\
            </div>\
            <div class="meta">\
              <span><i class="calendar icon"></i> ${attendingText(participant.attending)}</span>\
            </div>\
          </div>\
        </div>`;

      if (participant.attending == 1) {
        c1++;
        $goingCards.append(cardHtml);
      } else if (participant.attending == 2) {
        c2++;
        $notGoingCards.append(cardHtml);
      } else {
        c3++;
        $maybeCards.append(cardHtml);
      }

      // Update counts
      $goingCount.empty();
      $notGoingCount.empty();
      $maybeCount.empty();

      $goingCount.text(`(${c1})`);
      $notGoingCount.text(`(${c2})`);
      $maybeCount.text(`(${c3})`);
    });
  });
}

function getFoodMenuRealtimeUpdates() {
  foodMenuRef.orderBy('line').onSnapshot(function updateHandler(ss) {
    $foodMenuList.empty();

    ss.forEach(function eachDoc(doc) {
      let foodItem = doc.data();

      let descriptionHtml = '';
      if (foodItem.description != null && foodItem.description !== '') {
        descriptionHtml = `<div class="description">${foodItem.description}</div>`;
      }

      let quantityHtml = '';
      if (foodItem.quantity != null) {
        quantityHtml = ` <span class="quantity">x${foodItem.quantity}</span>`;
      }

      let foodMenuItemHtml = `<div class="item">\
        <i class="right angle icon"></i>\
        <div class="content">\
          <div class="header">${foodItem.name}${quantityHtml}\</div>\
          ${descriptionHtml}\
        </div>\
      </div>`

      $foodMenuList.append(foodMenuItemHtml);
    });
  });
}

// Code flow starts here

// scrollTopBtn click handler
$scrollTopBtn.on('click', function clickHandler() {
  $("html, body").animate({scrollTop: 0}, 400);
});

// main section button event handler
$('.hero a.ui.button').on('click', function clickHandler(e) {
  e.preventDefault();
  let offset = $($(this).attr('href')).offset();
  $("html, body").animate({scrollTop: offset.top}, 400);

});

// listen to scroll so as to know when to show/hide scrollTopBtn
$(window).scroll(function scrollHandler(e) {
  if ($('body').scrollTop() > 50 || $(document).scrollTop() > 50) {
        $scrollTopBtn.show();
    } else {
        $scrollTopBtn.hide();
    }
});

// initialise tab
 $('#attendance .menu .item').tab();

// start getting updates
getAttendanceRealtimeUpdates();
getFoodMenuRealtimeUpdates();
